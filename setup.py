#!/usr/bin/env python

from setuptools import setup, find_packages
import todo

setup(
    name='django-efficient-dic',
    description='A efficient dic backend',
    author='Ertuğrul ÇEtin',
    packages=find_packages(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ],
    include_package_data=True,
    zip_safe=False,
)
