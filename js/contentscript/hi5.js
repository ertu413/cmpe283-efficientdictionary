debug = false;
// The background page is asking us to find the words
var exact = true;

var colors = [
    ['black', '#FF6'],
    ['black', '#A0FFFF'],
    ['black', '#9F9'],
    ['black', '#F99'],
    ['black', '#F6F'],
    ['white', '#800'],
    ['white', '#0A0'],
    ['white', '#886800'],
    ['white', '#004699'],
    ['white', '#909']];

var wordsColorsStr = '';
var hilightedNodes = [];
var wordsColors = [];
var total = 0;
var currentNode = null;
var currentPos = -1;
var regex;

function resetGlobals() {
    wordsColorsStr = '';
    hilightedNodes = [];
    wordsColors = [];
    total = 0;
    currentNode = null;
    currentPos = null;
}


function normalizeWords(pearlsString) {
    return (pearlsString + '').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
}

function getWords(pearlsString) {
    if (debug) console.log(" Pearl String INI: " + pearlsString);
    pearls = pearlsString.length > 0 ? pearlsString.split(",") : [];
    new_pearls = [];
    for (i = 0; i < pearls.length; i++) {
        one_pearl = pearls[i].replace(/^\s+|\s+$/g, "");
        if (one_pearl != "")
            new_pearls[new_pearls.length] = one_pearl;
    }
    if (debug) console.log(" Pearl String END: " + new_pearls);
    return new_pearls;
}


function setInput(input) {
    input = input.replace(/^[^\w]+|[^\w]+$/g, "").replace(/[^\w'-]+/g, "|");
    var re = "(" + input + ")";
    re = "\\b" + re;
    regex = new RegExp(re, "i");
}


function walkElements(node, depth, textproc) {
    var skipre = /^(script|style|textarea|option)/i;
    var count = 0;
    while (node && depth > 0) {
        count++;
        if (count >= 10000000) {
            var handler = function () {
                walkElements(node, depth, textproc);
            };
            setTimeout(handler, 50);
            return;
        }

        if (node.nodeType == 1) { // ELEMENT_NODE
            if (!skipre.test(node.tagName) &&
                node.className != 'pearl-hilighted-word' &&
                node.className != 'pearl-current-hilighted-word' &&
                node.childNodes.length > 0) {
                node = node.childNodes[0];
                depth++;
                continue;
            }
        } else if (node.nodeType == 3 &&
            node.className != 'pearl-current-hilighted-word' &&
            node.className != 'pearl-hilighted-word') { // TEXT_NODE
            node = textproc(node);
        }

        if (node.nextSibling) {
            node = node.nextSibling;
        } else {
            while (depth > 0) {
                node = node.parentNode;
                depth--;
                if (node.nextSibling) {
                    node = node.nextSibling;
                    break;
                }
            }
        }
    }
}

function unhighlite() {

    for (i = 0; i < hilightedNodes.length; i++) {

        node = hilightedNodes[i];
        var realNode = node.previousSibling;
        var otherNode = node.nextSibling;

        if (realNode && otherNode) {
            realNode.data += node.textContent;
            realNode.data += otherNode.data;

            realNode.parentNode.removeChild(node);
            realNode.parentNode.removeChild(otherNode)
        }


    }
    if (debug) console.log('Unhighlite: ' + hilightedNodes.length);

    resetGlobals();

    return {total: 0};
}

function findPosXY(obj) {
    var curleft = 0, curtop = 0;
    if (obj && obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
            obj = obj.offsetParent;
        }
    }
    else if (obj && obj.x && obj.y) {
        curleft += obj.x;
        curtop += obj.y;
    }
    return {x: curleft, y: curtop};
}

var lastText = "";
var lastWords = "";
function hilightWords(wordsString) {
    var found;
    var node = document.body;
    var done = false;
    var wordsArray = getWords(wordsString);

    unhighlite();

    if (wordsArray.length > 0) {
        hiliteElement(document.body, wordsArray);
        if (debug) console.log("Hilight!!!");
        for (var f = 0; f < frames.length; f++) {
            try {
                hiliteElement(frames[f].document.body, wordsArray);
            } catch (err) {
                console.log("Chrome bug doesn't allow the suppression of this exception: https://code.google.com/p/chromium/issues/detail?id=17325")
            }
        }
    }
    // Ordering highlighted nodes from left to right, top to bottom.
    hilightedNodes = hilightedNodes.sort(
        function (nodea, nodeb) {
            posnodea = findPosXY(nodea);
            posnodeb = findPosXY(nodeb);
            return posnodea.y == posnodeb.y ? posnodea.x - posnodeb.x : posnodea.y - posnodeb.y;
        });

    if (debug) console.log('Frames ' + window.frames.length);

    for (var val in wordsColors)
        wordsColorsStr += val + ',' + wordsColors[val][0] + ',' + wordsColors[val][1] + ',';

    return {total: total};
}

function hiliteElement(elm, wordsArray) {
    if (!wordsArray || elm.childNodes.length == 0)
        return;

    var qre_inse_parts = []; // insensitive words
    var qre_sens_parts = [];  // sensitive words
    for (var i = 0; i < wordsArray.length; i++) {
        word = wordsArray[i];  //.toLowerCase();
        if (word.length > 2 && word[0] == "\"" && word[word.length - 1] == "\"")
            qre_sens_parts.push('\\b' + normalizeWords(word.slice(1, word.length - 1)) + '\\b');
        else if (exact)
            qre_inse_parts.push('\\b' + normalizeWords(word) + '\\b');
        else
            qre_inse_parts.push(normalizeWords(word));
    }

    qre_inse = qre_sens = regex;

    if (debug) {
        console.log(qre_inse);
        console.log(qre_sens);
    }

    curColor = 0;

    var textproc = function (node) {

        function paintPearlFound(val, pos) {

            var node2 = node.splitText(pos);
            node2.splitText(val.length);

            var span = node.ownerDocument.createElement('FONT');

            node.parentNode.replaceChild(span, node2);

            if (!(node && node.parentNode && ((node.parentNode.id.startsWith("seslichrome")) || (node.parentNode.className.startsWith("seslichrome")) ))) {

                span.className = 'pearl-hilighted-word';
                span.style.color = "#FFFFFF";
                span.style.background = "#FF8C00";
                span.style.fontStyle = "inherit";
            }

            $(span).on("click", function () {
                popBlinkUp(span);
            });

            span.appendChild(node2);
            hilightedNodes[hilightedNodes.length] = span;
            total++;
            return span;
        }

        var inse_match = qre_inse.exec(node.data);
        var sens_match = qre_sens.exec(node.data);
        var inse_matched = (inse_match !== null && inse_match[0].length > 0);
        var sens_matched = (sens_match !== null && sens_match[0].length > 0);

        if (inse_matched && !sens_matched ||
            (inse_matched && sens_matched &&
            inse_match.index <= sens_match.index)) {
            return paintPearlFound(inse_match[0].toLowerCase(), inse_match.index);
        } else if (sens_matched) {
            return paintPearlFound(sens_match[0].toLowerCase(), sens_match.index);
        }
        return node;
    };

    walkElements(elm.childNodes[0], 1, textproc);
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}