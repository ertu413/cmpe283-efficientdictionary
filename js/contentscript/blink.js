var b;

var selectedDic;

function popBlinkUp(span) {

    var meaning = span.innerText.toLowerCase();

    var foundedNode = allNodes[meaning];

    if (foundedNode) {

        $(document).mouseup(function (e) {

            var container = $("#seslichrome_dicContent");

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });

        $(document).keydown(function (e) {

            if (e.keyCode === 27) {

                var container = $("#seslichrome_dicContent");
                container.hide();
            }

        });

        b = $('body');

        checkOldPopUpIfExistDelete();
        renderNodeAndPopUp(foundedNode, span);
    }
}

function checkOldPopUpIfExistDelete() {

    var popUpMain = $("#seslichrome_dicContent");

    if (popUpMain[0]) {
        popUpMain.remove();
    }
}

function renderNodeAndPopUp(node, span) {

    var spanCoor = span.getBoundingClientRect();
    var elLeft = spanCoor.left;
    var elRight = spanCoor.right;
    var elTop = spanCoor.top;
    var elHeight = spanCoor.height;

    var elMid = ((elRight - elLeft) / 2) + elLeft;

    var leftPos = elMid - 175;
    leftPos = leftPos < 0 ? 3 : leftPos;

    var windowWidth = $(window).width();
    if (leftPos + 350 > windowWidth) {
        leftPos = windowWidth - 350;
    }

    var windowAndBodyTop = document.documentElement.scrollTop || document.body.scrollTop;
    var topPos = windowAndBodyTop + elTop - 220;
    if (topPos - windowAndBodyTop <= 0) {
        topPos += (elHeight + 220);
    }

    //console.log(topPos);

    var mainDiv = '<div id="seslichrome_dicContent" style="top:' + topPos + 'px; left:' + leftPos + 'px;"></div>';
    b.append(mainDiv);

    var jMainDiv = $("#seslichrome_dicContent");

    jMainDiv.bind('mousewheel DOMMouseScroll', function (e) {

        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        }
        else if (e.type == 'DOMMouseScroll') {
            scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
    });

    var turengDic = node.turengNode;
    var sesliSozlukDic = node.sesliSozlukNode;
    var dictionaryDic = node.dictionaryNode;

    var dicDiv = '<div id="dictionaryDiv">' +
        '<ul id="trDicDivId">' +
        '</ul>' +
        '</div><br>';

    jMainDiv.append(dicDiv);

    var blank = '<div style="clear: both;"></div>';
    jMainDiv.append(blank);

    var contentDiv = '<div id="contentDivId"></div>';

    jMainDiv.append(contentDiv);

    var jTr = $("#trDicDivId");

    if (turengDic) {

        jTr.append('<td>' +
            '<li><img id="jTurengIconId" style="height: 30px;width: 30px" class="seslichrome-pronunciation-audio" src="' + getLocalIcon("turBlack") + '"></li>' +
            '</td>');

        var jTurengIcon = $("#jTurengIconId");

        jTurengIcon.on("click", function () {
            renderIt("tureng", node);
        });

        jTurengIcon.on("mouseover", function () {
            $(this).attr('src', getLocalIcon("turengOrg"));
        });

        jTurengIcon.on("mouseout", function () {
            $(this).attr('src', getLocalIcon("turBlack"));
            makeDicImgColorful(selectedDic);
        });
    }

    if (sesliSozlukDic) {

        jTr.append('<td>' +
            '<li><img id="jSesliSozlukIconId" style="height: 30px;width: 30px" class="seslichrome-pronunciation-audio" src="' + getLocalIcon("sesliBlack") + '"></li>' +
            '</td>');

        var jSesliSozlukIcon = $("#jSesliSozlukIconId");

        jSesliSozlukIcon.on("click", function () {
            renderIt("seslisozluk", node);
        });

        jSesliSozlukIcon.on("mouseover", function () {
            $(this).attr('src', getLocalIcon("sesliOrg"));
        });

        jSesliSozlukIcon.on("mouseout", function () {
            $(this).attr('src', getLocalIcon("sesliBlack"));
            makeDicImgColorful(selectedDic);
        });
    }

    if (dictionaryDic) {

        jTr.append('<td>' +
            '<li><img id="jDicIconId" style="height: 30px;width: 30px" class="seslichrome-pronunciation-audio" src="' + getLocalIcon("dicBlack") + '"></li>' +
            '</td>');

        var jDicIcon = $("#jDicIconId");

        jDicIcon.on("click", function () {
            renderIt("dictionary", node);
        });

        jDicIcon.on("mouseover", function () {
            $(this).attr('src', getLocalIcon("dicOrg"));
        });

        jDicIcon.on("mouseout", function () {
            $(this).attr('src', getLocalIcon("dicBlack"));
            makeDicImgColorful(selectedDic);
        });
    }

    jTr.append('<td>' +
        '<div id="ertumSwitch" style="margin-right: auto" class="flipswitch">' +
        '<input type="checkbox" name="flipswitch" class="flipswitch-cb" id="fs" checked>' +
        '<label class="flipswitch-label" for="fs">' +
        '<div class="flipswitch-inner"></div>' +
        '<div class="flipswitch-switch"></div>' +
        '</label>' +
        '</div>' +
        '</td>');

    $('#ertumSwitch').find(':checkbox').click(function () {

        var $this = $(this);

        if (!($this.is(':checked'))) {

            chrome.runtime.sendMessage({blinkWordOff: node.word}, function (response) {
            });
            checkOldPopUpIfExistDelete();
        }
    });


    selectedDic = currentSite;
    renderIt(currentSite, node);

    jMainDiv.trigger("create");
}

function renderIt(dic, node) {

    $("#contentDivId").empty();

    switch (dic) {

        case "tureng":

            if (node.turengNode) {
                renderTureng(node);
            } else if (node.sesliSozlukNode) {
                renderSesliSozluk(node);
            } else {
                renderDictionary(node);
            }
            break;

        case "seslisozluk":

            if (node.sesliSozlukNode) {
                renderSesliSozluk(node);
            } else if (node.turengNode) {
                renderTureng(node);
            } else {
                renderDictionary(node);
            }
            break;

        default:

            if (node.dictionaryNode) {
                renderDictionary(node);
            } else if (node.turengNode) {
                renderTureng(node);
            } else {
                renderSesliSozluk(node);
            }
            break;
    }
}

function renderTureng(node) {

    abstractDicRenderer(node.turengNode);

    renderSavedMeanings(node);

    var resultDiv = $("#resultId");

    var meanings = node.turengNode.meanings;
    var category = "Try Me Bitch!!!";

    for (var i = 0; i < meanings.length; i++) {

        var itrCategory = meanings[i].category;
        var itrMeaning = meanings[i].meaning;
        var itrType = meanings[i].type;

        if (category !== itrCategory) {
            category = itrCategory;
            resultDiv.append('<div class="seslichrome_toTur"><span style="font-weight: bold;font-style: italic" class="seslichrome_turTitle">' + category + '</span><div class="seslichrome_toTurContent">');
        }

        resultDiv.append('<div class="seslichrome_toTurContent">' + getFormattedType(itrType) + itrMeaning + '<br></div>');
    }

    makeDicImgColorful("tureng");
}

function renderSesliSozluk(node) {

    abstractDicRenderer(node.sesliSozlukNode);

    renderSavedMeanings(node);

    var resultDiv = $("#resultId");

    var meanings = node.sesliSozlukNode.meanings;

    resultDiv.append('<div class="seslichrome_toTur"><span style="font-weight: bold" class="seslichrome_turTitle">' + 'Meanings' + '</span><div class="seslichrome_toTurContent">');

    for (var i = 0; i < meanings.length; i++) {

        var meaning = meanings[i].meaning;
        var category = meanings[i].category;

        resultDiv.append('<div class="seslichrome_toTurContent">' + meaning + getFormattedCategoryForSesliSozluk(category) + '<br></div>');
    }

    makeDicImgColorful("seslisozluk");
}

function renderDictionary(node) {

    abstractDicRenderer(node.dictionaryNode);

    renderSavedMeanings(node);

    var resultDiv = $("#resultId");

    var meanings = node.dictionaryNode.meanings;

    for (var i = 0; i < meanings.length; i++) {

        var meaning = meanings[i];
        resultDiv.append('<p style="font-style: italic"><b>' + (i + 1) + ') </b>' + meaning + '</p>');
    }

    makeDicImgColorful("dictionary");
}

function abstractDicRenderer(someDicNode) {

    var hasSoundPaths = !($.isEmptyObject(someDicNode.soundsPaths));

    var contentDiv = $("#contentDivId");

    if (hasSoundPaths) {

        var soundDiv = '<div id="soundDiv">' +
            '<ul id="soundUlId">' +
            '</ul>' +
            '</div>';

        contentDiv.append(soundDiv);

        var ul = $("#soundUlId");

        var usSound = someDicNode.soundsPaths["us"];
        var ukSound = someDicNode.soundsPaths["uk"];
        var auSound = someDicNode.soundsPaths["au"];

        if (usSound) {
            ul.append('<li><img id="usSoundId" style="height: 35px;width: 35px" class="seslichrome-pronunciation-audio" src="' + getLocalIcon("blackUs") + '">' +
                '<audio id="usAudioSoundId"></audio></li>');

            var usSoundImg = $("#usSoundId");

            usSoundImg.on("mouseover", function () {
                $(this).attr('src', getLocalIcon("us"))
            });

            usSoundImg.on("mouseout", function () {
                $(this).attr('src', getLocalIcon("blackUs"))
            });

            createAudioPlayer("usSoundId", usSound);
        }

        if (ukSound) {
            ul.append('<li><img id="ukSoundId" style="height: 35px;width: 35px" class="seslichrome-pronunciation-audio" src="' + getLocalIcon("blackUk") + '">' +
                '<audio id="ukAudioSoundId"></audio></li>');

            var ukSoundImg = $("#ukSoundId");

            ukSoundImg.on("mouseover", function () {
                $(this).attr('src', getLocalIcon("uk"))
            });

            ukSoundImg.on("mouseout", function () {
                $(this).attr('src', getLocalIcon("blackUk"))
            });

            createAudioPlayer("ukSoundId", ukSound);
        }

        if (auSound) {
            ul.append('<li><img id="auSoundId" style="height: 35px;width: 35px" class="seslichrome-pronunciation-audio" src="' + getLocalIcon("blackAu") + '">' +
                '<audio id="auAudioSoundId"></audio></li>');

            var auSoundImg = $("#auSoundId");

            auSoundImg.on("mouseover", function () {
                $(this).attr('src', getLocalIcon("au"))
            });

            auSoundImg.on("mouseout", function () {
                $(this).attr('src', getLocalIcon("blackAu"))
            });

            createAudioPlayer("auSoundId", auSound);
        }

        ul.append('<li><div id="seslichrome_word">' + someDicNode.word + '</div></li>');

    } else {
        contentDiv.append('<li><div id="seslichrome_word">' + someDicNode.word + '</div></li>');
    }

    contentDiv.append('<div id="seslichrome_close">✖</div><div style="clear: both;"></div>');

    $("#seslichrome_close").on("click", function () {
        $("#seslichrome_dicContent").remove();
    });
}

function renderSavedMeanings(node) {

    var resultDiv = '<div id="resultId" class="seslichrome_tr"></div>';

    var contentDiv = $("#contentDivId");
    contentDiv.append(resultDiv);

    var savedMeanings = node.savedMeanings;
    if (savedMeanings && savedMeanings.length > 0) {

        var div = '<div class="seslichrome_toTur"><span style="font-style: italic;color: #005599;font-weight: bold" class="seslichrome_turTitle">Saved Meanings</span>' +
            '<div class="seslichrome_toTurContent">';

        for (var i = 0; i < savedMeanings.length; i++) {
            div += savedMeanings[i] + '<br>';
        }

        div += '</div></div>';

        $("#resultId").append(div);
    }
}

function makeDicImgColorful(chosenImg) {

    var tur = $("#jTurengIconId");
    var ses = $("#jSesliSozlukIconId");
    var dic = $("#jDicIconId");

    if (chosenImg === "tureng") {

        if (ses[0]) {
            ses.attr('src', getLocalIcon("sesliBlack"));
        }

        if (dic[0]) {
            dic.attr('src', getLocalIcon("dicBlack"));
        }

        tur.attr('src', getLocalIcon("turengOrg"));

        selectedDic = "tureng";

    } else if (chosenImg === "seslisozluk") {

        if (tur[0]) {
            tur.attr('src', getLocalIcon("turBlack"));
        }

        if (dic[0]) {
            dic.attr('src', getLocalIcon("dicBlack"));
        }

        ses.attr('src', getLocalIcon("sesliOrg"));

        selectedDic = "seslisozluk";
    } else {

        if (tur[0]) {
            tur.attr('src', getLocalIcon("turBlack"));
        }

        if (ses[0]) {
            ses.attr('src', getLocalIcon("sesliBlack"));
        }

        dic.attr('src', getLocalIcon("dicOrg"));

        selectedDic = "dictionary";
    }
}

function getFormattedType(type) {

    if (type) {
        return "(" + type + ") ";
    }

    return "";
}

function getFormattedCategoryForSesliSozluk(type) {

    if (type) {
        return " ~ (" + type + ") ";
    }

    return "";
}

function getLocalIcon(name) {
    return chrome.extension.getURL("icons/buttons/" + name + ".png");
}

function createAudioPlayer(buttonId, soundPath) {

    $("#" + buttonId).on("click", function (e) {

        try {
            var audio = new Audio(soundPath);
            audio.play();
        } catch (e) {
            console.log(e);
        }
    });
}