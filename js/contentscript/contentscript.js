var allNodes;

var currentSite;

var a = new Date().getTime();


var observeDOM = (function () {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
        eventListenerSupported = window.addEventListener;

    return function (obj, callback) {
        if (MutationObserver) {
            // define a new observer
            var obs = new MutationObserver(function (mutations, observer) {
                if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
                    callback();
            });
            // have the observer observe foo for changes in children
            obs.observe(obj, {childList: true, subtree: true});
        }
        else if (eventListenerSupported) {
            obj.addEventListener('DOMNodeInserted', callback, false);
            obj.addEventListener('DOMNodeRemoved', callback, false);
            obj.addEventListener('DOMSubtreeModified', callback, false);
        }
    }
})();

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {


    if (request.method == "getSelection") {

        var selectedText = window.getSelection().toString();
        sendResponse({data: selectedText});
    }

    if (request.type == "switchOff") {

        unhighlite();

        allNodes = null;

        sendResponse({});
    }

    if (request.type == "highlight") {

        var text = request.wordsString;

        allNodes = request.allNodes;

        currentSite = request.currentSite;

        setInput(text);

        highlightIfExist(text);

        bindObserver($('div#viewerContainer')[0], text);
        bindObserver(document.body, text);

        sendResponse({});
    }
});

function bindObserver(element, text) {

    if (element) {

        observeDOM(element, function (e) {

            var b = new Date().getTime();
            if (b - a > 2500) {

                //console.log('ERTUM dom changed !!');

                setTimeout(function () {
                    highlightIfExist(text);
                }, 100);
            }
            a = b;
        });
    }
}

function highlightIfExist(text) {

    if (allNodes) {
        hilightWords(text);
    }
}