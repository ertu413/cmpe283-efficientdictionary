function DictionaryParser() {
    this.word = null;
    this.meanings = [];
    this.isEnglish = true;
    this.isNotFound = false;
    this.isNoNetwork = false;
    this.soundsPaths = {};
}

DictionaryParser.prototype.setWord = function (word) {
    this.word = word;
};

DictionaryParser.prototype.getResultAsJSON = function () {

    var me = this;

    $.ajaxSetup({async: false});

    $.get("http://dictionary.reference.com/browse/" + me.word, function (data) {

        var html = data;

        var parser = new DOMParser();
        var dom = parser.parseFromString(html, "text/html");

        var meanings = dom.getElementsByClassName("def-pbk ce-spot");

        if (meanings) {

            var meaning = meanings[0];

            var sectionChilds = meaning.children;

            for (var i = 1; i < sectionChilds.length; i++) {

                var defSet = sectionChilds[i];
                var defSetChilds = defSet.children;

                me.meanings.push(defSetChilds[1].innerText.trim());
            }

            var soundPath = dom.getElementsByClassName("audio-wrapper cts-disabled")[0];

            if (soundPath) {
                me.soundsPaths["us"] = soundPath.children[1].getAttribute("href").trim();
            }
        }

    }).fail(function () {

        $.get("https://www.google.com.tr/search?q=deneme", function (data) {
            if (data) {
                me.isNotFound = true;
            }
        }).fail(function () {
            me.isNoNetwork = true;
        });

    });

    return this;
};