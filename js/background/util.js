Array.prototype.contains = function (s) {
    return this.indexOf(s) !== -1
};

Array.prototype.getSortedAsc = function () {
    return this.sort(function (a, b) {
        return a - b;
    })
};

Array.prototype.getSortedDesc = function () {
    return this.sort(function (a, b) {
        return b - a;
    })
};

function Set() {
    this.array = [];
}

Set.prototype.add = function (value) {

    var alreadyHasVal = this.array.contains(value);

    if (!alreadyHasVal) {
        this.array.push(value);
        return true;
    }

    return false;
};

Set.prototype.values = function () {
    return this.array.values();
};

Set.prototype.getArray = function () {
    return this.array;
};


function getReplacedTurkishToLowerCase(str) {

    return str.replace(/Ğ/g, "ğ")
        .replace(/Ü/g, "ü")
        .replace(/Ş/g, "ş")
        .replace(/İ/g, "i")
        .replace(/Ö/g, "ö")
        .replace(/Ç/g, "ç")
        .replace(/\"/g, "'");
}

function getLowerCaseString(str) {

    var s = getReplacedTurkishToLowerCase(str);
    return s.toLowerCase();
}

function getFilteredInput(input) {

    if (input && input.length > 0) {

        var r = new RegExp("\\s+", 'g');

        var temp = getReplacedTurkishToLowerCase(input).toLowerCase().trim();

        return temp.replace(r, ' ');

    } else {
        return "";
    }
}