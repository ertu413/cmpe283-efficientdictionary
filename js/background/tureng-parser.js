function TurengParser() {
    this.word = null;
    this.meanings = [];
    this.suggestions = [];
    this.isEnglish = false;
    this.isNotFound = false;
    this.isNoNetwork = false;
    this.soundsPaths = {};
}

function MeaningNode() {
    this.category = null;
    this.meaning = null;
    this.type = null;
}

MeaningNode.prototype.setCategory = function (category) {
    this.category = category;
};

MeaningNode.prototype.setMeaning = function (meaning) {
    this.meaning = meaning;
};

MeaningNode.prototype.setType = function (type) {
    this.type = type;
};

TurengParser.prototype.setWord = function (word) {
    this.word = word;
};


//TODO first~aid! tarzı bişiy girince first aid e yakın kelimenin anlamlarını gösteriyor belki ek geliştirme yapılır.
TurengParser.prototype.getResultAsJSON = function () {

    var me = this;

    //TODO belki clasic ajax kullanıp async:false demek lazım olabilir !!
    $.ajaxSetup({async: false});

    $.get("http://tureng.com/en/turkish-english/" + me.word, function (data) {

        var html = data;

        var parser = new DOMParser();
        var dom = parser.parseFromString(html, "text/html");

        var suggestion = dom.getElementsByClassName("suggestion-list")[0];

        if (suggestion) {

            var childrens = suggestion.children;

            var childrensLen = childrens.length;
            if (childrensLen > 1) {

                for (var x = 0; x < childrensLen - 1; x++) {

                    var val = childrens[x];
                    me.suggestions.push(val.innerText.trim());
                }

            } else {
                me.isNotFound = true;
            }

        } else {

            var table = dom.getElementsByClassName("table table-hover table-striped searchResultsTable");

            var firstTable = table[0];

            var tbody = firstTable.children[0];

            var tbodyChildrens = tbody.children;

            var firstTr = tbodyChildrens[0];
            me.isEnglish = (firstTr.children[2].innerText) === "English";

            for (var i = 3; i < tbodyChildrens.length; i++) {

                var tr = tbodyChildrens[i];

                var trChildrens = tr.children;

                var meaning = new MeaningNode();

                var trChildrensLen = trChildrens.length;
                if (trChildrensLen < 5) {
                    continue;
                }

                for (var j = 1; j < trChildrens.length; j++) {

                    var th = trChildrens[j];

                    if (j == 1) {
                        meaning.setCategory(th.innerText.trim());
                        continue;
                    }

                    if (me.isEnglish) {

                        if (j == 2) {

                            var thChildrens = th.children;
                            if (thChildrens && thChildrens.length > 1) {
                                meaning.setType(thChildrens[1].innerText.trim());
                            } else {
                                meaning.setType("");
                            }
                        } else if (j == 3) {
                            meaning.setMeaning(th.innerText.trim());
                        }
                    } else {

                        if (j == 3) {

                            var thChildrens = th.children;
                            if (thChildrens && thChildrens.length > 1) {
                                meaning.setMeaning(thChildrens[0].innerText.trim());
                                meaning.setType(thChildrens[1].innerText.trim());
                            } else {
                                meaning.setMeaning(th.innerText.trim());
                                meaning.setType("");
                            }
                        }
                    }
                }

                me.meanings.push(meaning);
            }

            if (me.isEnglish) {

                var soundUS = dom.getElementById("turengVoiceENTRENus");
                if (soundUS) {
                    me.soundsPaths["us"] = soundUS.children[0].src.trim();
                }

                var soundUK = dom.getElementById("turengVoiceENTRENuk");
                if (soundUK) {
                    me.soundsPaths["uk"] = soundUK.children[0].src.trim();
                }

                var soundAU = dom.getElementById("turengVoiceENTRENau");
                if (soundAU) {
                    me.soundsPaths["au"] = soundAU.children[0].src.trim();
                }
            }
        }
    }).fail(function () {
        me.isNoNetwork = true;
    });

    return this;
};