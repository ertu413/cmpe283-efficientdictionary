from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^$', 'todo.views.list_lists', name="dic-lists"),
)
