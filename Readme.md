#Efficient Dictionary (Chrome Extension)

Efficient dictionary allows you to search words offline if you have already looked for that word.
And it highlights words on web pages so you can click highlighted word and see meanings in a pop-up.
It increases your productivity.
You can set your shortcut for fast searching.

