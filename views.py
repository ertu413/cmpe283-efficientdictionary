from django.shortcuts import render_to_response
from todo.models import Item, List, Comment
from todo.forms import AddListForm, AddItemForm, EditItemForm, AddExternalItemForm, SearchForm
from todo import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.contrib.auth.decorators import user_passes_test
from django.db import IntegrityError
from django.db.models import Q
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required


import datetime

# Need for links in email templates
current_site = Site.objects.get_current()


def check_user_allowed(user):
    """
    test for user_passes_test decorator
    """
    if settings.STAFF_ONLY:
        return user.is_authenticated() and user.is_staff
    else:
        return user.is_authenticated()


@user_passes_test(check_user_allowed)
def list_lists(request):
    """
    Homepage view - list of lists a user can view, and ability to add a list.
    """
    thedate = datetime.datetime.now()
    searchform = SearchForm(auto_id=False)

    # Make sure user belongs to at least one group.
    group_count = request.user.groups.all().count()
    if group_count == 0:
        messages.error(request, "You do not yet belong to any groups. Ask your administrator to add you to one.")

        # Only show lists to the user that belong to groups they are members of.
    # Superusers see all lists
    if request.user.is_superuser:
        list_list = List.objects.all().order_by('group', 'name')
    else:
        list_list = List.objects.filter(group__in=request.user.groups.all).order_by('group', 'name')

    # Count everything
    list_count = list_list.count()

    # Note admin users see all lists, so count shouldn't filter by just lists the admin belongs to
    if request.user.is_superuser:
        item_count = Item.objects.filter(completed=0).count()
    else:
        item_count = Item.objects.filter(completed=0).filter(list__group__in=request.user.groups.all()).count()

    return render_to_response('dic/popup.html', locals(), context_instance=RequestContext(request))
