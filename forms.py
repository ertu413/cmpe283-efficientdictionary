from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User, Group
from todo.models import Item, List


class AddListForm(ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(AddListForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = Group.objects.filter(user=user)

    class Meta:
        model = List
        exclude = []

class SearchForm(forms.Form):
    """Search."""

    q = forms.CharField(
        widget=forms.widgets.TextInput(attrs={'size': 35})
    )
